"""sales_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from .views import api_sales_person, api_customers, api_sales, api_autos, api_sales_person_sales, api_autos_available

urlpatterns = [
    path("sales/new/", api_sales, name="api_sales"),
    path("sales/all/", api_sales, name="api_sales"),
    path("sales/<int:id>/", api_sales_person_sales, name="api_sales_person_sales"),
    path("customers/", api_customers, name="api_customers"),
    path("salespersons/", api_sales_person, name="api_sales_person"),
    path("salespersons/<int:id>/", api_sales_person, name="api_sales_person"),
    path("autos/", api_autos, name="api_autos"),
    path("autos/available/", api_autos_available, name="api_autos_available"),
]
 