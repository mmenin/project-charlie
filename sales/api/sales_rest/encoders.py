from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, Customer, Sale

class AutomobileEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
    "model",
    "year",
    "color",
    "vin",
    "manufacturer",
    "sold",
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_num",
    ]
    
class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "price",
    ]
    def get_extra_data(self, o):
        return {"sales_person":o.sales_person.name,
                "customer":o.customer.name,
                "vin":o.automobile.vin,
                "sold":o.automobile.sold,
                }
        
class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "price",
    ]
    def get_extra_data(self, o):
        return {"sales_person":o.sales_person.name,
                "employee_num":o.sales_person.employee_num,
                "customer":o.customer.name,
                "vin":o.automobile.vin,
                }