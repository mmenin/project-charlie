import React from 'react';


class TechnicianForm extends React.Component {

        state = {
            name: '',
            employee_number: '',
        };

    handleSubmit = async(event) => {
        this.setState({saving: true});
        event.preventDefault();
        const data = {...this.state};
        delete data.saving;

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                name: '',
                employee_number: '',
                saving: false,
            };
            this.setState(cleared);
        }
        else {
            console.log('Could not add technician, check inputs and try again')
        }
    }

    handleChange = (event) => {
        const value = event.target.value;
        this.setState({[event.target.name]: value})
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1> Add a Technician</h1>
                <form onSubmit={this.handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={this.handleChange} value={this.state.employee_number} placeholder="employee_number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                    <label htmlFor="employee_number">Employee Number</label>
                </div>
                <button className="btn btn-primary">Add Technician</button>
                </form>
            </div>
            </div>
        </div>
        )
    }
}

export default TechnicianForm;
