
import React, { useState, useEffect, } from 'react';

function AutoForm(){
    const [models,setModels]=useState([]);
    const [modelId,setModel]=useState('');
    const [color,setColor]=useState('');
    const [year,setYear]=useState(0);
    const [vin,setVin]=useState('');

    const handleModelChange = (e) =>{ setModel(e.target.value); }
    const handleColorChange = (e) =>{ setColor(e.target.value); }
    const handleYearChange = (e) =>{ setYear(e.target.value); }
    const handleVinChange = (e) =>{ setVin(e.target.value); }

    const loadModels = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
          let data = await response.json();
          setModels( data.models.map( (model) => ({
              name: model.name,
              id: model.id,
              manufacturer: model.manufacturer.name
              }))
          );
        }
        else if (!response.ok) {
          console.log("Error retrieving model list.")
        }
      }

      const submitForm = async () => {
        let data = {
            model_id: modelId,
            color: color,
            year: year,
            vin: vin
        }

        const autoUrl ='http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModel('');
         }
        else {console.log("Unable to create automobile record")}
      }

    useEffect( () => {
        loadModels();
      }, [] );


    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1> Add a Vehicle Record</h1>
                <div className="form-floating mb-3">
                    <input onChange={handleYearChange} value={year} placeholder="year" required type="number"
                        name="year" id="year" className="form-control" />
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="color" required type="text"
                        name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text"
                        name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">VIN</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleModelChange} required name="modelId"
                            id="modelId" className='form-select' value={modelId}>
                    <option value="">Choose a model</option>
                    {models?.map(model => {
                        return (
                        <option value={model.id} key={model.id}>
                            {model.manufacturer} {model.name}
                        </option>
                        );
                    })}
                    </select>
                </div>
                    <button onClick={submitForm} className="btn btn-primary">Create</button>
            </div>
            </div>
        </div>
    )

}


export default AutoForm;
