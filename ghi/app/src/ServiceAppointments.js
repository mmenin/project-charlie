import React, {useState, useEffect} from 'react';


function ServiceAppointments() {
    const [appointments, setAppointments] = useState ([])
    let [vip] = useState ('')

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if(response.ok) {
            const data = await response.json()
            setAppointments(data.appointments.filter(appointment => appointment.completed === false))
        }
        else {
            console.log('Could not load appointment data')
        }

    }

    function cancelService(id) {
        fetch(`http://localhost:8080/api/appointments/${id}`,{
            method: 'DELETE'
        })
        setAppointments(appointments.filter(appointment => appointment.id !== id))
    }

    function completeService(id) {
        fetch(`http://localhost:8080/api/appointments/${id}/`,{
            method: 'PATCH',
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                completed: true
            }),
        })
        .then(response => response.json())
        setAppointments(appointments.filter(appointment => appointment.id !== id))
    }

    useEffect(() => {
        fetchData ();
    }, [])

    return (

        <>
         <div className="px-4 py-5 my-5 text-left">
        <h1 className="display-5 fw-bold">Service Appointments</h1>
        </div>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Customer Name</th>
                    <th>Vip Status</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Service</th>
                </tr>
            </thead>
            <tbody>
                {appointments.map(appointment => {

                    if (appointment.vip === true) {
                        vip = 'VIP'
                    }
                    else {
                        vip = ''
                    }

                    return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.owner}</td>
                            <td>{vip}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.technician.name}</td>
                            <td>{appointment.service}</td>
                            <td><button onClick={() => cancelService(appointment.id)}>Cancel</button></td>
                            <td><button onClick={() => completeService(appointment.id)}>Finished</button></td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        </>
        )
}

export default ServiceAppointments;
