import React, {useState, useEffect} from "react";


function ServiceHistory() {
    const [appointments, setAppointments] = useState ([])
    const [searchText, setSearchText] = useState ('')
    const handleChange = (e) => {
        setSearchText(e.target.value)
    }

    const fetchData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/')
        if (response.ok) {
            const data = await response.json()
        setAppointments(data.appointments.filter(appointment => appointment.completed === true))
        }
        else {
            console.log('Could not load appointment data')
        }

    }

    useEffect(() => {
        fetchData ();
    }, [])

    return (

        <>
        <div className="px-4 py-5 my-1 text-left">
        <h1 className="display-5 fw-bold">Service History</h1>
        </div>
        <input type="search" placeholder="Search Vin" value={searchText} onChange={handleChange} />
        <table className="table table-striped">
        <thead>
            <tr>
                <th>VIN</th>
                <th>Customer Name</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Service</th>
            </tr>
        </thead>
        <tbody>
            {appointments.filter(appointment => appointment.vin===searchText).map(appointment => {
                return (
                    <tr key={appointment.id}>
                        <td>{appointment.vin}</td>
                        <td>{appointment.owner}</td>
                        <td>{appointment.date}</td>
                        <td>{appointment.time}</td>
                        <td>{appointment.technician.name}</td>
                        <td>{appointment.service}</td>
                    </tr>
                )
            })}
        </tbody>
    </table>
        </>

    )

}

export default ServiceHistory;
